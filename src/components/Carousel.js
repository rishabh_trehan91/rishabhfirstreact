import {Component} from "react";
import logo1 from '../images/cakes.jpg';
import logo2 from '../images/cakes_2.jpg';

class Carousel extends Component {
    render() {
        return (
            <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" className="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                </ol>
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img src={logo1} className="d-block w-100" alt="Image 1" />
                            <div className="carousel-caption d-none d-md-block">
                                <h5>It smells freshly baked</h5>
                                <p>We deliver the best of quality.</p>
                            </div>
                    </div>
                    <div className="carousel-item">
                        <img src={logo2} className="d-block w-100" alt="Image 2" />
                            <div className="carousel-caption d-none d-md-block">
                                <h5>Making Your Special Day More Special</h5>
                                <p>We deliver the best of quality.</p>
                            </div>
                    </div>
                </div>
                <a className="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        )
    }
}

export default Carousel;